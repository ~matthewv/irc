#!/usr/bin/env python
#
# Bot logic:
# Andrew Walkingshaw <andrew@lexical.org.uk>
#
# IRC framework:
# Joel Rosdahl <joel@rosdahl.net>
#
# Contributors:
# Peter Corbett <ptc24@cam.ac.uk>
# Matthew Vernon <matthew@debian.org>
#
# Stephen Early <steve@greenend.org.uk> mostly deleted stuff
# 
# This file is part of Acrobat.
#
# Acrobat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# Acrobat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Acrobat; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA.

"""
Acrobat - an extensible, minmalist irc bot.
"""

import string, sys
from ircbot import SingleServerIRCBot
from irclib import nm_to_n, irc_lower

class Acrobat(SingleServerIRCBot):
    def __init__(self,config):
	server=config.server
	port=config.port
	nickname=config.nickname
        SingleServerIRCBot.__init__(self,
                                    [(server, port)], nickname, nickname)
        self.channel = config.channel
        self.owner = config.owner
        self.revision = "$Revision: 1.1 $" # global version number
        self.config = config
        
    ## EVENT HANDLERS
            
    def on_welcome(self, conn, evt):
        conn.join(self.channel)

    def on_privmsg(self, conn, evt):
        self.do_command(nm_to_n(evt.source()), evt.arguments()[0])
        
    def on_pubmsg(self, conn, evt):
        payload = evt.arguments()[0]
        nc = string.split(payload, " ", 1)
        if len(nc) > 1 and (irc_lower(nc[0]).startswith(
            irc_lower(self.connection.get_nickname()))):
            self.do_command(nm_to_n(evt.source()), nc[1].strip(), public = 1)
        elif len(payload)>1:
            self.do_command(nm_to_n(evt.source()), payload.strip(), public = 1)
    # General query handler
    def do_command(self, nick, cmd, public=0):
	self.config.command(self,cmd,nick,self.connection,public)

    # Convenience function - reply to a public message publicly, or
    # a private message privately
    def automsg(self,public,nick,msg):
	if public:
	    self.connection.privmsg(self.channel,msg)
	else:
	    self.connection.notice(nick, msg)

def main():
    if len(sys.argv) < 2:
	print "acrobat: provide configuration module name on command line"
	sys.exit(1)
    c=__import__(sys.argv[1])
    # Override configuration items from the rest of the command line
    for opt in sys.argv[2:]:
        (key,value)=opt.split("=")
        c.__dict__[key] = value
    bot = Acrobat(c)
    bot.start()

if __name__ == "__main__":
    main()
