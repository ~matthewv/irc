			  Servus, an IRC bot

This file documents (briefly) the development and usage of the Servus
bot from the perspective of the bot operator. Servus.html contains the
user-facing documentation. 

* Invocation *

"python acrobat.py Servus-chiark" is the basic usage. For long-running
instances, you probably want to run using nohup (and redirecting
stdout somewhere sensible). Equally, if you want to change the owner,
server, channel, nickname and the like, you can edit
Servus-chiark.py or append the relevant overrides to the command line,
e.g.: 
python acrobat.py Servus-chiark nickname=testbot channel=\#test owner=MyNick

* Known Issues *

- "define" is currently broken, because google have buggered around
  with the results to a define: query. There are at least two output
  formats now, too: cf "define:zealot" and "define:VFS", for examples
  of each.

- Servus doesn't see things people emit with /me. This is a problem
  for the URL-tracking code, and needs fixing.

- Servus will only emit ASCII. It's not clear to me that this is
  wrong, but if we move to everyone having UTF8-capable irc clients,
  it would be nice to fix it such that it emits ASCII when it can and
  UTF8 otherwise

- Servus can only deal with being on a single channel at once

* Dependencies *

Servus depends on some things that aren't in standard python (or
included in its source). You can install them system-wide or add the
--prefix command to setup.py install (and make sure PYTHONPATH is
sensible). 

i) oauth2

https://github.com/simplegeo/python-oauth2

in debian stable as python-oauth2

ii) httplib2

http://code.google.com/p/httplib2/

in debian oldstable+ as python-httplib2

iii) simplejson

http://cheeseshop.python.org/pypi/simplejson

or in standard python2.6 and above

iv) python-twitter (which requires all of the above)

https://github.com/bear/python-twitter
