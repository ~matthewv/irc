# This file is part of Acrobat.
#
# Acrobat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# Acrobat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Acrobat; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA.

# Andrew Walkingshaw <andrew@lexical.org.uk>
# Peter Corbett <ptc24@cam.ac.uk>
# Matthew Vernon <matthew@debian.org>
# Stephen Early <steve@greenend.org.uk>
# Richard Kettlewell <rjk@greenend.org.uk

# Acrobat configuration file

# The following definitions are required to be present in this module:
# You can also override them on the command-line
# e.g. python acrobat.py Servus-chiark nickname=testbot channel=\#test owner=MyNick
server = "chiark"
port = 6667
nickname = "Servus"
channel = "#chiark"
owner = "Emperor"
# Also a function called "command"; see later.

# Everything else in this file is configuration-specific.

import os, time, re, twitter, subprocess, sys, os.path

# Most command implementations are stored in a separate module.
import commands as c

# This fishpond is shared between trouts and flirts.  It doesn't have to be;
# you can define as many ponds as you like.
class Fish (c.FishPond):
	cur_fish=5
	max_fish=5
	nofish_time=60
	fish_time_inc=60
	fish_inc=2
	Boring_Git='Nobody'

fish = Fish()

# load the "blame" details for a file
def loadblame(filename):
    p=subprocess.Popen(["git","blame","-s",filename],
                       stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out,err=p.communicate()
    if len(err)>0:
        sys.exit("git blame failure: %s" % err)
    bdb={}
    lines=out.split("\n")
    for line in lines:
        l=line.split()
        if len(line.strip())>0:
            commit=l[0]
            thing=' '.join(l[2:])
            bdb[thing]=commit
    keys=bdb.keys()
    return bdb,keys,filename
    
# load a file full of flirts or trouts
def __load(filename):
    try:
	f = open(filename, "r")
        r = [l.strip() for l in f.readlines() if l.find("%s") != -1]
        f.close()
    except IOError:
        r = [ "doesn't know what to do about %s." ]
    return r

# (troutlist,selftroutmsg,DoSmsg,notargetmsg,nofishmsg,fishpond,selftroutprob)
troutcfg = (
	__load("trouts"),
	' (at the instigation of %s)',
	"Sorry, but %s is being a spoilsport.",
	"Who do you wish me to trout?",
	"Fish stocks exhausted.",
	fish,
	0.1,
	loadblame("trouts"),
	)

flirtcfg = (
	__load("flirts"),
	' (but %s is their secret admirer)',
	"Sorry, but %s made me take Holy Orders.",
	"Who do you wish me to flirt with?",
	"My libido is over-used!",
	fish,
	0.1,
	loadblame("flirts"),
	)

slashcfg= ( 
	__load("slashes"),
	' (while %s watches)',
	"Sorry, but %s stole my pen.",
	"Who do you want to slash?",
	"I have writer's block!",
	fish,
	0.1,
	loadblame("slashes")
	)

# Hacky command to output the current fishpond state
def fishq(bot, cmd, nick, conn, public,f):
	from irclib import irc_lower
	if not public and irc_lower(nick) == irc_lower(bot.owner):
		state=("Fishpond state: cur_fish=%d, max_fish=%d, nofish_time=%d, "
		       +"fish_time_inc=%d, fish_inc=%d, DoS=%d, Boring_Git=%s, "
		       +"quotatime=%d")%(f.cur_fish,f.max_fish,f.nofish_time,
					 f.fish_time_inc,f.fish_inc,f.DoS,f.Boring_Git,
					 f.quotatime)
		bot.automsg(public,nick,state)
                    
# Karma implementation
import cPickle
karmafilename = "chiark-karma-"+channel
# load the karma db
try:
    f = open(karmafilename, "r")
    karmadb = cPickle.load(f)
    f.close()
except IOError:
    karmadb = {}
# Modify karma
def karma(cmd, amount):
    thing=cmd.split()[0][:-2].lower()
    if karmadb.has_key(thing):
        karmadb[thing] += amount
    else:
        karmadb[thing] = amount
    savekarma()
def savekarma():
    tmp = "%s.tmp" % karmafilename
    try:
        f = open(tmp, "w")
        cPickle.dump(karmadb, f)
        f.close()
	os.rename(tmp, karmafilename)
    except IOError, e:
        sys.stderr.write("error writing karma: %s" % e)

def quit(bot,cmd,nick,conn,public):
    c.quitq(bot,cmd,nick,conn,public)
def reload(bot,cmd,nick,conn,public):
    c.reloadq(bot,cmd,nick,conn,public)

# initialise the urldb on startup
urldb={}
lastexp=time.time()
#expire urls if not asked about or seen for >71 hours
expirelen=71*60*60
#do an expiry run every hour
expirevery=60*60


#path where Oauth details are kept
twioauthpath=os.path.expanduser("~/private/servus_twapi_oauth.txt")

try:
  f=open(twioauthpath,"r")
  for line in f:
    if line[0]=='#':
      continue
    key,val=map(str.strip,line.split(':'))
    if key == "consumer_key":
      twoaapck = val
    elif key == "consumer_secret":
      twoaapcs = val
    elif key == "access_token":
      twoapat = val
    elif key == "access_token_secret":
      twoapats = val
    else:
      raise ValueError, "Invalid line in twitter auth details file %s" % line
  f.close()
  twitapi = twitter.Api(consumer_key = twoaapck,
			consumer_secret = twoaapcs,
			access_token_key = twoapat,
			access_token_secret = twoapats,
                        tweet_mode = "extended")
except IOError:
# non-authenticated twitter api instance
  twitapi = twitter.Api()

# Command processing: whenever something is said that the bot can hear,
# "command" is invoked and must decide what to do.  This configuration
# defines a couple of special cases (for karma) but is otherwise driven
# by a dictionary of commands.

commands = {"karma": (c.karmaq,karmadb),
	    "karmalist": (c.listkeysq,karmadb),
	    "karmadel": (c.karmadelq,karmadb),
            "info": (c.infoq,karmadb),
            "trout": (c.troutq,troutcfg),
	    "slash": (c.slashq, slashcfg),
	    "rot13": c.rot13q,
	    "fish": (fishq,fish),
            "flirt": (c.troutq,flirtcfg),
	    "quiet": (c.nofishq,fish),
            "reload": reload,
            "quit": quit,
	    "die": quit,
	    "define": c.defineq,
            "google": c.googleq,
	    "url": (c.urlq,urldb),
	    "nsfw": (c.nsfwq,urldb),
	    "nws": (c.nsfwq,urldb),
	    "units": c.unitq,
	    "currency":c.currencyq,
            "blame": (c.blameq,fish, [troutcfg,flirtcfg,slashcfg]),
	    "help": c.helpq,
            "say": c.sayq,
            "do": c.doq, 
            "twit": (c.twitterq,twitapi) }
# disconnect and hop annoy people
#            "disconnect": c.disconnq,
#            "hop": c.disconnq }
commands["list"]=(c.listkeysq,commands,True)

triggers = ("!", "~") # what character should the bot be invoked by:
                      # eg !trout, ~trout etc.

def command(bot, cmd, nick, conn, public):
    global urldb,lastexp,expirelen,expirevery,twitapi
    ours=0
    try:
	    if public and cmd[0] in triggers:
		    ours=1
		    cmd=cmd[1:]
	    if not public:
		    ours=1
	    command = cmd.split()[0]
    except IndexError:
	    command=""

    t=time.time()
    if t - lastexp > expirevery:
	    c.urlexpire(urldb,expirelen)
	    lastexp=t

    if public:
      if c.urlre.search(cmd) and command.lower()!="url":
        c.dourl(bot,conn,nick,cmd,urldb)

    # karma: up
    if command.endswith("++"):
        karma(cmd,1)
    # karma: down
    if command.endswith("--"):
        karma(cmd,-1)

    if ours and command.lower() in commands.keys():
	e=commands[command.lower()]
	if callable(e):
	    e(bot,cmd,nick,conn,public)
	else:
	    e[0](bot,cmd,nick,conn,public,*e[1:])
