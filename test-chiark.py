# This file is part of Acrobat.
#
# Acrobat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# Acrobat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Acrobat; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA.

# Andrew Walkingshaw <andrew@lexical.org.uk>
# Peter Corbett <ptc24@cam.ac.uk>
# Matthew Vernon <matthew@debian.org>
# Stephen Early <steve@greenend.org.uk>
# Richard Kettlewell <rjk@greenend.org.uk

# Acrobat configuration file

# The following definitions are required to be present in this module:
server = "rapun"
port = 6667
nickname = "testbot"
channel = "#test"
owner = "Emperor"
# Also a function called "command"; see later.

# Everything else in this file is configuration-specific.

import os, time

# Most command implementations are stored in a separate module.
import commands as c

# This fishpond is shared between trouts and flirts.  It doesn't have to be;
# you can define as many ponds as you like.
class Fish (c.FishPond):
	cur_fish=5
	max_fish=5
	nofish_time=60
	fish_time_inc=60
	fish_inc=2
	Boring_Git='Nobody'

fish = Fish()

# load a file full of flirts or trouts
def __load(filename):
    try:
	f = open(filename, "r")
        r = [l.strip() for l in f.readlines() if l.find("%s") != -1]
        f.close()
    except IOError:
        r = [ "doesn't know what to do about %s." ]
    return r

# (troutlist,selftroutmsg,DoSmsg,notargetmsg,nofishmsg,fishpond,selftroutprob)
troutcfg = (
	__load("trouts"),
	' (at the instigation of %s)',
	"Sorry, but %s is being a spoilsport.",
	"Who do you wish me to trout?",
	"Fish stocks exhausted.",
	fish,
	0.1)

flirtcfg = (
	__load("flirts"),
	' (but %s is their secret admirer)',
	"Sorry, but %s made me take Holy Orders.",
	"Who do you wish me to flirt with?",
	"My libido is over-used!",
	fish,
	0.1)

slashcfg= ( 
	__load("slashes"),
	' (while %s watches)',
	"Sorry, but %s stole my pen.",
	"Who do you want to slash?",
	"I have writer's block!",
	fish,
	0.1)

# Hacky command to output the current fishpond state
def fishq(bot, cmd, nick, conn, public,f):
	from irclib import irc_lower
	if not public and irc_lower(nick) == irc_lower(bot.owner):
		state=("Fishpond state: cur_fish=%d, max_fish=%d, nofish_time=%d, "
		       +"fish_time_inc=%d, fish_inc=%d, DoS=%d, Boring_Git=%s, "
		       +"quotatime=%d")%(f.cur_fish,f.max_fish,f.nofish_time,
					 f.fish_time_inc,f.fish_inc,f.DoS,f.Boring_Git,
					 f.quotatime)
		bot.automsg(public,nick,state)
                    
# Karma implementation
import cPickle
karmafilename = "chiark-test-karma"
# load the karma db
try:
    f = open(karmafilename, "r")
    karmadb = cPickle.load(f)
    f.close()
except IOError:
    karmadb = {}
# Modify karma
def karma(cmd, amount):
    thing=cmd.split()[0][:-2].lower()
    if karmadb.has_key(thing):
        karmadb[thing] += amount
    else:
        karmadb[thing] = amount
    savekarma()
def savekarma():
    tmp = "%s.tmp" % karmafilename
    try:
        f = open(tmp, "w")
        cPickle.dump(karmadb, f)
        f.close()
	os.rename(tmp, karmafilename)
    except IOError, e:
        sys.stderr.write("error writing karma: %s" % e)

def quit(bot,cmd,nick,conn,public):
    c.quitq(bot,cmd,nick,conn,public)
def reload(bot,cmd,nick,conn,public):
    c.reloadq(bot,cmd,nick,conn,public)

#The game...
class game:
	trigger="Servus"
	grace=time.time()
	minlose=24*60*60 #1 day
	maxlose=14*minlose #2 weeks
	losetime=time.time()

# Command processing: whenever something is said that the bot can hear,
# "command" is invoked and must decide what to do.  This configuration
# defines a couple of special cases (for karma) but is otherwise driven
# by a dictionary of commands.

commands = {"karma": (c.karmaq,karmadb),
	    "karmalist": (c.listkeysq,karmadb),
	    "karmadel": (c.karmadelq,karmadb),
            "info": (c.infoq,karmadb),
            "help": (c.helpq),
            "trout": (c.troutq,troutcfg),
	    "slash": (c.slashq, slashcfg),
	    "rot13": c.rot13q,
	    "fish": (fishq,fish),
            "flirt": (c.troutq,flirtcfg),
	    "quiet": (c.nofishq,fish),
            "reload": reload,
            "quit": quit,
	    "die": quit,
            "google": c.googleq,
	    "units": c.unitq,
#	    "define": c.defineq,
#	    "game": (c.gameq,game),
            "say": c.sayq,
            "do": c.doq }
# disconnect and hop annoy people
#            "disconnect": c.disconnq,
#            "hop": c.disconnq }
commands["list"]=(c.listkeysq,commands)

triggers = ("!", "~") # what character should the bot be invoked by:
                      # eg !trout, ~trout etc.

def command(bot, cmd, nick, conn, public):
    ours=0
    try:
	    if public and cmd[0] in triggers:
		    ours=1
		    cmd=cmd[1:]
	    if not public:
		    ours=1
	    command = cmd.split()[0]
    except IndexError:
	    command=""
    # karma: up
    if command.endswith("++"):
        karma(cmd,1)
    # karma: down
    if command.endswith("--"):
        karma(cmd,-1)

    if ours and command.lower() in commands.keys():
	e=commands[command.lower()]
	if callable(e):
	    e(bot,cmd,nick,conn,public)
	else:
	    e[0](bot,cmd,nick,conn,public,*e[1:])
    elif public:
        if cmd.find("GAME")!=-1: #someone else lost
	    grace.grace=time.time()+60*20
	elif cmd.find(game.trigger)!=-1: #we lost!
	    c.gameq(bot,"pad "+game.trigger,bot.owner,conn,False,game)
	elif time.time()>game.losetime: #we randomly lost, take new trigger
	    c.gameq(bot,cmd,bot.owner,conn,False,game)
	    
